package com.learning.repairshop.util;

public enum RepairStatus {
        PENDING(1),
        COMPLETED(2),
        CANCELLED(3);

        public final int value;

        RepairStatus(int value) {
                this.value = value;
        }
}
