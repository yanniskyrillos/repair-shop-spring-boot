package com.learning.repairshop.util;

public enum UserType {
    ADMIN(1),
    USER(2);

    public final int value;

    UserType(int value) {
        this.value = value;
    }
}
