package com.learning.repairshop.controller;

import com.learning.repairshop.domain.Repair;
import com.learning.repairshop.domain.Vehicle;
import com.learning.repairshop.service.RepairService;
import com.learning.repairshop.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/vehicles")
public class VehicleController {

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private RepairService repairService;

    @GetMapping
    public List<Vehicle> findAll() {
        return vehicleService.findAll();
    }

    @GetMapping(path = "/{id}")
    public Vehicle findById(@PathVariable(value = "id") Long id) {
        return vehicleService.findById(id);
    }

    @GetMapping(path = "/{id}/repairs")
    public List<Repair> findAllRepairsByVehicle(@PathVariable(value = "id") Long id) {
        return repairService.findAllRepairsByVehicle(id);
    }

    @PostMapping
    public void create(@RequestBody Vehicle vehicle) {
        vehicleService.create(vehicle);
    }

    @PutMapping
    public void updateById(@RequestBody Vehicle vehicle) {
        vehicleService.updateById(vehicle);
    }

    @DeleteMapping(path = "/{id}/delete")
    public void delete(@PathVariable(value = "id") Long id) {
        vehicleService.delete(id);
    }

}
