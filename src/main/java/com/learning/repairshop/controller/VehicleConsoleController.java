package com.learning.repairshop.controller;

import com.learning.repairshop.domain.Vehicle;
import com.learning.repairshop.service.RepairService;
import com.learning.repairshop.service.VehicleService;

import java.util.*;

public class VehicleConsoleController implements MenuRenderer {

    private Map<Integer, Runnable> menuActions = new HashMap<>();
    private List<String> menuList = new ArrayList<>();
    VehicleService vehicleService = new VehicleService();
    RepairService repairService = new RepairService();

    public VehicleConsoleController() {
        menuList.add("1. Create a Vehicle");
        menuList.add("2. Delete a Vehicle by ID");
        menuList.add("3. Update a Vehicle by ID");
        menuList.add("4. Search for a Vehicle by ID");
        menuList.add("5. Show all Vehicles");
        menuList.add("6. Show all repairs of a Vehicle");
        menuList.add("0. Return to Main Menu");
        menuActions.put(1, this::create);
        menuActions.put(2, this::delete);
        menuActions.put(3, this::update);
        menuActions.put(4, this::findById);
        menuActions.put(5, this::findAll);
        menuActions.put(6, this::findAllRepairsByVehicle);
        menuActions.put(0, this::exit);
    }

    public void printMenu() {
        String input;
        List<String> vehicleOptions = Arrays.asList("0", "1", "2", "3", "4", "5", "6");
        do {
            menuList.forEach(System.out::println);
            input = MainControllerNew.scanner.nextLine();
        } while (! vehicleOptions.contains(input));
        menuActions.get(Integer.parseInt(input)).run();
    }

    public void create() {
        System.out.println("Enter ID");
        Long id = Long.parseLong(MainControllerNew.scanner.nextLine());
        System.out.println("Enter plate number");
        String plateNumber = MainControllerNew.scanner.nextLine();
        System.out.println("Enter brand");
        String brand = MainControllerNew.scanner.nextLine();
        System.out.println("Enter model");
        String model = MainControllerNew.scanner.nextLine();
        System.out.println("Enter color");
        String color = MainControllerNew.scanner.nextLine();
        System.out.println("Enter price");
        double price = Double.parseDouble(MainControllerNew.scanner.nextLine());
        System.out.println("Enter ID of the owner");
        Long userId = Long.parseLong(MainControllerNew.scanner.nextLine());
        System.out.println("Enter year");
        int year = Integer.parseInt(MainControllerNew.scanner.nextLine());
        System.out.println("Enter month");
        int month = Integer.parseInt(MainControllerNew.scanner.nextLine());
        System.out.println("Enter day");
        int day = Integer.parseInt(MainControllerNew.scanner.nextLine());
        Vehicle vehicle = new Vehicle(id, plateNumber, brand, model, color, price, userId, year, month, day);
        vehicleService.create(vehicle);
    }

    public void delete() {
        System.out.println("Enter ID to delete");
        Long id = Long.parseLong(MainControllerNew.scanner.nextLine());
        vehicleService.delete(id);
    }

    public void update() {
        System.out.println("Enter ID of an existing vehicle");
        Long id = Long.parseLong(MainControllerNew.scanner.nextLine());
        while (!vehicleService.idExists(id)) {
            System.out.println("The ID you entered is invalid");
            System.out.println("Enter ID of an existing vehicle");
            id = Long.parseLong(MainControllerNew.scanner.nextLine());
        }
        System.out.println("Enter plate number");
        String plateNumber = MainControllerNew.scanner.nextLine();
        System.out.println("Enter brand");
        String brand = MainControllerNew.scanner.nextLine();
        System.out.println("Enter model");
        String model = MainControllerNew.scanner.nextLine();
        System.out.println("color");
        String color = MainControllerNew.scanner.nextLine();
        System.out.println("price");
        double price = Double.parseDouble(MainControllerNew.scanner.nextLine());
        System.out.println("Enter ID of the owner");
        Long userId = Long.parseLong(MainControllerNew.scanner.nextLine());
        System.out.println("Enter year");
        int year = Integer.parseInt(MainControllerNew.scanner.nextLine());
        System.out.println("Enter month");
        int month = Integer.parseInt(MainControllerNew.scanner.nextLine());
        System.out.println("Enter day");
        int day = Integer.parseInt(MainControllerNew.scanner.nextLine());
        Vehicle vehicle = new Vehicle(id, plateNumber, brand, model, color, price, userId, year, month, day);
        vehicleService.updateById(vehicle);
    }

    public void findById() {
        System.out.println("Enter Vehicle ID");
        Long id = Long.parseLong(MainControllerNew.scanner.nextLine());
        System.out.println(vehicleService.findById(id));
    }

    public void findAll() {
        System.out.println(vehicleService.findAll());
    }

    public void findAllRepairsByVehicle() {
        System.out.println("Enter Vehicle ID");
        Long id = Long.parseLong(MainControllerNew.scanner.nextLine());
        System.out.println(repairService.findAllRepairsByVehicle(id));
    }

    public void exit() {
        System.out.println("Going back to main");
        MainControllerNew mainControllerNew = new MainControllerNew();
        mainControllerNew.printMenu();
    }
}
