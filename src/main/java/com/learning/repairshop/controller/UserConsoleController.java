package com.learning.repairshop.controller;

import com.learning.repairshop.domain.User;
import com.learning.repairshop.service.UserService;
import java.util.*;

public class UserConsoleController implements MenuRenderer{

    private Map<Integer, Runnable> menuActions = new HashMap<>();
    private List<String> menuList = new ArrayList<>();
    private UserService userService = new UserService();

    public UserConsoleController() {
        menuList.add("1. Create a User");
        menuList.add("2. Delete a User by ID");
        menuList.add("3. Update a User by ID");
        menuList.add("4. Search for a User by email");
        menuList.add("5. Show all Users");
        menuList.add("6. Login");
        menuList.add("7. Show all vehicles of a User by Id");
        menuList.add("8. Show all repairs of all vehicles of a User by ID");
        menuList.add("0. Return to Main Menu");
        menuActions.put(1, this::create);
        menuActions.put(2, this::delete);
        menuActions.put(3, this::update);
        menuActions.put(4, this::findByEmail);
        menuActions.put(5, this::findAll);
        menuActions.put(6, this::login);
        menuActions.put(7, this::findAllVehiclesByUser);
        menuActions.put(8, this::findAllRepairsOfAllVehiclesByUser);
        menuActions.put(0, this::exit);
    }

    @Override
    public void printMenu() {
        String input;
        List<String> userOptions = Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8");
        do {
            menuList.forEach(System.out::println);
            input = MainControllerNew.scanner.nextLine();
        } while(! userOptions.contains(input));
        menuActions.get(Integer.parseInt(input)).run();
    }

    public void create() {
        System.out.println("Enter ID");
        Long id = null;
        do try {
            id = Long.parseLong(MainControllerNew.scanner.nextLine());
        }
        catch (NumberFormatException e) {
            System.out.println("Invalid input");
            System.out.println("Enter ID");
        }
        while (id == null);
        String email;
        do {
            System.out.println("Enter email");
            email = MainControllerNew.scanner.nextLine();
        } while (email.isEmpty());
        String password = null;
        do {
            System.out.println("Enter password");
            password = MainControllerNew.scanner.nextLine();
        } while (password.isEmpty());
        String firstName = null;
        do {
            System.out.println("Enter first name");
            firstName = MainControllerNew.scanner.nextLine();
        } while (firstName.isEmpty());
        String lastName = null;
        do {
            System.out.println("Enter last name");
            lastName = MainControllerNew.scanner.nextLine();
        } while (lastName.isEmpty());
        String address = null;
        do {
            System.out.println("Enter address");
            address = MainControllerNew.scanner.nextLine();
        } while (address.isEmpty());
        int afm = 0;
        do {
            System.out.println("Enter AFM");
            afm = Integer.parseInt(MainControllerNew.scanner.nextLine());
        } while (afm == 0);
        System.out.println("Enter user type");
        int type = Integer.parseInt(MainControllerNew.scanner.nextLine());
        User user = new User(id, email, password, firstName, lastName, address, afm, type);
        userService.create(user);
    }

    public void delete() {
        System.out.println("Enter ID to delete");
        Long id = Long.parseLong(MainControllerNew.scanner.nextLine());
        userService.delete(id);
    }

    public void update() {
        System.out.println("Enter ID of an existing user");
        Long id = Long.parseLong(MainControllerNew.scanner.nextLine());
        while (!userService.idExists(id)) {
            System.out.println("The ID you entered is invalid");
            System.out.println("Enter ID of an existing user");
            id = Long.parseLong(MainControllerNew.scanner.nextLine());
        }
        System.out.println("Enter email");
        String email = MainControllerNew.scanner.nextLine();
        System.out.println("Enter password");
        String password = MainControllerNew.scanner.nextLine();
        System.out.println("Enter first name");
        String firstName = MainControllerNew.scanner.nextLine();
        System.out.println("Enter last name");
        String lasstName = MainControllerNew.scanner.nextLine();
        System.out.println("Enter address");
        String address = MainControllerNew.scanner.nextLine();
        System.out.println("Enter AFM");
        int afm = Integer.parseInt(MainControllerNew.scanner.nextLine());
        System.out.println("Enter user type");
        int type = Integer.parseInt(MainControllerNew.scanner.nextLine());
        User user = new User(id, email, password, firstName, lasstName, address, afm, type);
        userService.updateById(user);
    }

    public void findByEmail() {
        System.out.println("Enter email");
        String email = MainControllerNew.scanner.nextLine();
        System.out.println(userService.findByEmail(email));
    }

    public void findAll() {
        userService.findAll().forEach(System.out::println);
    }

    public void login() {
        System.out.println("Enter email");
        String email = MainControllerNew.scanner.nextLine();
        System.out.println("Enter password");
        String password = MainControllerNew.scanner.nextLine();
        System.out.println(userService.login(email, password));
    }

    public void findAllVehiclesByUser() {
        System.out.println("Enter User ID");
        Long id = Long.parseLong(MainControllerNew.scanner.nextLine());
        System.out.println(userService.findAllVehiclesByUser(id));
    }

    public void findAllRepairsOfAllVehiclesByUser() {
        System.out.println("Enter User ID");
        Long id = Long.parseLong(MainControllerNew.scanner.nextLine());
        System.out.println(userService.findAllRepairsOfAllVehiclesByUser(id));
    }

    public void exit() {
        System.out.println("Going back to main");
        MainControllerNew mainControllerNew = new MainControllerNew();
        mainControllerNew.printMenu();
    }
}
