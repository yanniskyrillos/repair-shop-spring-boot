package com.learning.repairshop.controller;

import java.util.*;

public class MainControllerNew implements MenuRenderer {

    static Scanner scanner = new Scanner(System.in);
    private List<String> menuList = new ArrayList<>();
    private Map<Integer, Runnable> menuActions = new HashMap<>();

    public MainControllerNew() {
        menuList.add("Select");
        menuList.add("1. User");
        menuList.add("2. Vehicle");
        menuList.add("3. Repair");
        menuList.add("4. Part");
        menuList.add("0. Exit");
        menuActions.put(1, this::actionUser);
        menuActions.put(2, this::actionVehicle);
        menuActions.put(3, this::actionRepair);
        menuActions.put(4, this::actionPart);
        menuActions.put(0, this::exit);
    }

    @Override
    public void printMenu() {
        String input;
        List<String> mainOptions = Arrays.asList("0", "1", "2", "3", "4");
        do {
            do {
                menuList.forEach(System.out::println);
                input = scanner.nextLine();
            } while (!mainOptions.contains(input));
            menuActions.get(Integer.parseInt(input)).run();
        } while (! input.equals("0"));
    }

    private void actionUser() {
        UserConsoleController userConsoleController = new UserConsoleController();
        userConsoleController.printMenu();
    }

    private void actionVehicle() {
        VehicleConsoleController vehicleConsoleController = new VehicleConsoleController();
        vehicleConsoleController.printMenu();
    }

    private void actionRepair() {
        RepairConsoleController repairConsoleController = new RepairConsoleController();
        repairConsoleController.printMenu();
    }

    private void actionPart() {
        PartConsoleController partConsoleController = new PartConsoleController();
        partConsoleController.printMenu();
    }

    private void exit() {
        System.out.println("Exiting...");
    }
}
