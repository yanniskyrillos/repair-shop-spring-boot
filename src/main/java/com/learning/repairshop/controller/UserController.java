package com.learning.repairshop.controller;

import com.learning.repairshop.domain.Repair;
import com.learning.repairshop.domain.User;
import com.learning.repairshop.domain.Vehicle;
import com.learning.repairshop.service.UserService;
import com.learning.repairshop.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private VehicleService vehicleService;

    @GetMapping
    public List<User> findAll() {
        return userService.findAll();
    }

    @GetMapping(path = "/{email}")
    public User findByEmail(@PathVariable(value = "email") String email) {
        return userService.findByEmail(email);
    }

    @GetMapping(path = "/{id}/vehicles")
    public List<Vehicle> findAllVehiclesByUser(@PathVariable(value = "id") Long id) {
        return vehicleService.findAllVehiclesByUser(id);
    }

    @GetMapping(path = "/{id}/repairs")
    public List<Repair> findAllRepairsOfAllVehiclesByUser(@PathVariable(value = "id") Long id) {
        return userService.findAllRepairsOfAllVehiclesByUser(id);
    }

    @PostMapping
    public void create(@RequestBody User user) {
        userService.create(user);
    }

    @PutMapping
    public void updateById(@RequestBody User user) {
        userService.updateById(user);
    }

    @DeleteMapping(path = "/{id}/delete")
    public void delete(@PathVariable(value = "id") Long id){
        userService.delete(id);
    }

}
