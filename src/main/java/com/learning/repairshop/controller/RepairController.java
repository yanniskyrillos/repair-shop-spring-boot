package com.learning.repairshop.controller;

import com.learning.repairshop.domain.Repair;
import com.learning.repairshop.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/repairs")
public class RepairController {

    @Autowired
    private RepairService repairService;

    @GetMapping
    public List<Repair> findAll() {
        return repairService.findAll();
    }

    @GetMapping(path = "/{id}")
    public Repair findById(@PathVariable(value = "id") Long id) {
        return repairService.findById(id);
    }

    @GetMapping(path = "/{id}/cost")
    public double calculateTotalRepairCost(@PathVariable(value = "id") Long id) {
        return repairService.calculateTotalRepairCost(id);
    }

    @PostMapping
    public void create(@RequestBody Repair repair) {
        repairService.create(repair);
    }

    @PutMapping
    public void updateById(@RequestBody Repair repair) {
        repairService.updateById(repair);
    }

    @DeleteMapping(path = "/{id}/delete")
    public void delete(@PathVariable( value = "id") Long id) {
        repairService.delete(id);
    }


}
