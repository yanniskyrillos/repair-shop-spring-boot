package com.learning.repairshop.controller;

import com.learning.repairshop.domain.Repair;
import com.learning.repairshop.service.RepairService;
import com.learning.repairshop.util.RepairStatus;

import java.util.*;

public class RepairConsoleController implements MenuRenderer {

    private Map<Integer, Runnable> menuActions = new HashMap<>();
    private List<String> menuList = new ArrayList<>();
    RepairService repairService = new RepairService();

    public RepairConsoleController() {
        menuList.add("1. Create a Repair");
        menuList.add("2. Delete a Repair by ID");
        menuList.add("3. Update a Repair by ID");
        menuList.add("4. Search for a Repair by ID");
        menuList.add("5. Show all Repairs");
        menuList.add("6. Calculate cost");
        menuList.add("0. Return to Main Menu");
        menuActions.put(1, this::create);
        menuActions.put(2, this::delete);
        menuActions.put(3, this::update);
        menuActions.put(4, this::findById);
        menuActions.put(5, this::findAll);
        menuActions.put(6, this::calculateTotalRepairCost);
        menuActions.put(0, this::exit);
    }

    public void printMenu() {
        String input;
        List<String> repairOptions = Arrays.asList("0", "1", "2", "3", "4", "5", "6");
        do {
            menuList.forEach(System.out::println);
            input = MainControllerNew.scanner.nextLine();
        } while (!repairOptions.contains(input));
        menuActions.get(Integer.parseInt(input)).run();
    }

    public void create() {
        System.out.println("Enter ID");
        Long id = null;
        do try {
            id = Long.parseLong(MainControllerNew.scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Invalid input");
            System.out.println("Enter ID");
        }
        while (id == null);
        String stringRepairStatus;
        do {
            System.out.println("Enter repair status\nChoices are:\nPENDING\nCOMPLETED\nCANCELLED");
            stringRepairStatus = MainControllerNew.scanner.nextLine();
        } while (!("PENDING".equals(stringRepairStatus)
                || "COMPLETED".equals(stringRepairStatus)
                || "CANCELLED".equals(stringRepairStatus)));
        RepairStatus repairStatus = RepairStatus.valueOf(stringRepairStatus);
        System.out.println("Enter Vehicle ID");
        Long vehicleId = null;
        do try {
            vehicleId = Long.parseLong(MainControllerNew.scanner.nextLine());
        }
        catch (NumberFormatException e) {
            System.out.println("Invalid input");
            System.out.println("Enter Vehicle ID");
        }
        while (vehicleId == null);
        System.out.println("Enter cost");
        double cost = Double.parseDouble(MainControllerNew.scanner.nextLine());
        System.out.println("Enter year");
        int year = Integer.parseInt(MainControllerNew.scanner.nextLine());
        System.out.println("Enter month");
        int month = Integer.parseInt(MainControllerNew.scanner.nextLine());
        System.out.println("Enter day");
        int day = Integer.parseInt(MainControllerNew.scanner.nextLine());
        System.out.println("Enter hour");
        int hour = Integer.parseInt(MainControllerNew.scanner.nextLine());
        System.out.println("Enter minute");
        int minute = Integer.parseInt(MainControllerNew.scanner.nextLine());
        Repair repair = new Repair(id, repairStatus, vehicleId, cost, year, month, day, hour, minute);
        repairService.create(repair);
    }

    public void delete() {
        System.out.println("Enter ID to delete");
        Long id = Long.parseLong(MainControllerNew.scanner.nextLine());
        repairService.delete(id);
    }

    public void update() {
        System.out.println("Enter ID of an existing Repair");
        Long id = Long.parseLong(MainControllerNew.scanner.nextLine());
        while (!repairService.idExists(id)) {
            System.out.println("Enter a valid Repair ID");
            id = Long.parseLong(MainControllerNew.scanner.nextLine());
        }
        String stringRepairStatus;
        do {
            System.out.println("Enter repair status\nChoices are:\nPENDING\nCOMPLETED\nCANCELLED");
            stringRepairStatus = MainControllerNew.scanner.nextLine();
        } while ("PENDING".equals(stringRepairStatus)
                || "COMPLETED".equals(stringRepairStatus)
                || "CANCELLED".equals(stringRepairStatus));
        RepairStatus repairStatus = RepairStatus.valueOf(stringRepairStatus);
        System.out.println("Enter Vehicle ID");
        Long vehicleId = Long.parseLong(MainControllerNew.scanner.nextLine());
        System.out.println("Enter cost");
        double cost = Double.parseDouble(MainControllerNew.scanner.nextLine());
        System.out.println("Enter year");
        int year = Integer.parseInt(MainControllerNew.scanner.nextLine());
        System.out.println("Enter month");
        int month = Integer.parseInt(MainControllerNew.scanner.nextLine());
        System.out.println("Enter day");
        int day = Integer.parseInt(MainControllerNew.scanner.nextLine());
        System.out.println("Enter hour");
        int hour = Integer.parseInt(MainControllerNew.scanner.nextLine());
        System.out.println("Enter minute");
        int minute = Integer.parseInt(MainControllerNew.scanner.nextLine());
        Repair repair = new Repair(id, repairStatus, vehicleId, cost, year, month, day, hour, minute);
        repairService.updateById(repair);
    }

    public void findById() {
        System.out.println("Enter ID of a Repair");
        Long id = Long.parseLong(MainControllerNew.scanner.nextLine());
        System.out.println(repairService.findById(id));
    }

    public void findAll() {
        System.out.println(repairService.findAll());
    }

    public void calculateTotalRepairCost() {
        System.out.println("Enter ID of an existing Repair");
        Long id = Long.parseLong(MainControllerNew.scanner.nextLine());
        while (!repairService.idExists(id)) {
            System.out.println("Enter a valid Repair ID");
            id = Long.parseLong(MainControllerNew.scanner.nextLine());
        }
        System.out.println(repairService.calculateTotalRepairCost(id));
    }

    public void exit() {
        System.out.println("Going back to main");
        MainControllerNew mainControllerNew = new MainControllerNew();
        mainControllerNew.printMenu();
    }
}
