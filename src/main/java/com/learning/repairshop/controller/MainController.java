/*
package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainController {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String mainInput;
        do {
            System.out.println("Select\n1. User\n2. Vehicle\n3. Repair\n4. Part\n0. Exit");
            mainInput = scanner.nextLine();
            while (invalidMainInput(mainInput)) {
                System.out.println("Select\n1. User\n2. Vehicle\n3. Repair\n4. Part\n0. Exit");
                mainInput = scanner.nextLine();
            }
            String input = mainInput;
            if ("1".equals(input)) {
                UserConsoleController userController = new UserConsoleController();
//                userController.printMenu();
                input = String.valueOf(userController.printMenu());
//                input = scanner.nextLine();
                while (invalidUserMenuInput(input)) {
                    input = String.valueOf(userController.printMenu());
//                    input = scanner.nextLine();
                }
                if (input.equals("1")) {
                    userController.create();
                } else if (input.equals("2")) {
                    List<Runnable> runnableList = new ArrayList<>();

                    Runnable r = userController::delete;
                    runnableList.add(r);
                    runnableList.get(0).run();


//                    userController.delete();
                } else if (input.equals("3")) {
                    userController.update();
                } else if (input.equals("4")) {
                    userController.findByEmail();
                } else if (input.equals("5")) {
                    userController.findAll();
                } else if (input.equals("6")) {
                    userController.login();
                } else if (input.equals("7")) {
                    userController.findAllVehiclesByUser();
                } else if (input.equals("8")) {
                    userController.findAllRepairsOfAllVehiclesByUser();
                }
            }
            else if (input.equals("2")) {
                VehicleConsoleController vehicleController = new VehicleConsoleController();
                vehicleController.printMenu();
                input = scanner.nextLine();
                while (invalidVehicleMenuInput(input)) {
                    vehicleController.printMenu();
                    input = scanner.nextLine();
                }
                if (input.equals("1")) {
                    vehicleController.create();
                }
                else if (input.equals("2")) {
                    vehicleController.delete();
                }
                else if (input.equals("3")) {
                    vehicleController.update();
                }
                else if (input.equals("4")) {
                    vehicleController.findById();
                }
                else if (input.equals("5")) {
                    vehicleController.findAll();
                }
                else if (input.equals("6")) {
                    vehicleController.findAllRepairsByVehicle();
                }
            }
            else if (input.equals("3")) {
                RepairConsoleController repairController = new RepairConsoleController();
                repairController.printMenu();
                input = scanner.nextLine();
                while (invalidRepairMenuInput(input)) {
                    repairController.printMenu();
                    input = scanner.nextLine();
                }
                if (input.equals("1")) {
                    repairController.create();
                }
                else if (input.equals("2")) {
                    repairController.delete();
                }
                else if (input.equals("3")) {
                    repairController.update();
                }
                else if (input.equals("4")) {
                    repairController.findById();
                }
                else if (input.equals("5")) {
                    repairController.findAll();
                }
                else if (input.equals("6")) {
                    repairController.calculateTotalRepairCost();
                }
            }
            else if (input.equals("4")) {
                PartConsoleController partController = new PartConsoleController();
                partController.printMenu();
                input = scanner.nextLine();
                while (invalidPartMenuInput(input)) {
                    partController.printMenu();
                    input = scanner.nextLine();
                }
                if (input.equals("1")) {
                    partController.create();
                }
                else if (input.equals("2")) {
                    partController.delete();
                }
                else if (input.equals("3")) {
                    partController.update();
                }
                else if (input.equals("4")) {
                    partController.findById();
                }
                else if ("5".equals(input)) {
                    partController.findAll();
                }
            }
        } while (!(mainInput.equals("0")));
    }
    private static boolean invalidUserMenuInput(String input) {
        return !(input.equals("1") || input.equals("2") || input.equals("3") || input.equals("4") ||
                input.equals("5") || input.equals("6") || input.equals("7") || input.equals("8")
                || input.equals("0"));
    }

    private static boolean invalidMainInput(String input) {
        return !(input.equals("1") || input.equals("2") || input.equals("3") || input.equals("4") || input.equals("0"));
    }

    private static boolean invalidVehicleMenuInput(String input) {
        return !(input.equals("1") || input.equals("2") || input.equals("3") || input.equals("4") ||
                input.equals("5") || input.equals("6") || input.equals("0"));
    }

    private static boolean invalidRepairMenuInput(String input) {
        return !(input.equals("1") || input.equals("2") || input.equals("3") || input.equals("4") ||
                input.equals("5") || input.equals("6") || input.equals("0"));
    }

    private static boolean invalidPartMenuInput(String input) {
        return !(input.equals("1") || input.equals("2") || input.equals("3") || input.equals("4") ||
                input.equals("5") || input.equals("0"));
    }
}
*/
