package com.learning.repairshop.controller;

import static com.learning.repairshop.controller.MainControllerNew.scanner;

import com.learning.repairshop.domain.Part;
import com.learning.repairshop.service.PartService;

import java.util.*;

public class PartConsoleController implements MenuRenderer {

    private Map<Integer, Runnable> menuActions = new HashMap<>();
    private List<String> menuList = new ArrayList<>();
    PartService partService = new PartService();

    public PartConsoleController() {
        menuList.add("1. Create a Part");
        menuList.add("2. Delete a Part by ID");
        menuList.add("3. Update a Part by ID");
        menuList.add("4. Search for a Part by ID");
        menuList.add("5. Show all Parts");
        menuList.add("0. Return to Main Menu");
        menuActions.put(1, this::create);
        menuActions.put(2, this::delete);
        menuActions.put(3, this::update);
        menuActions.put(4, this::findById);
        menuActions.put(5, this::findAll);
        menuActions.put(0, this::exit);
    }

    public void printMenu() {
        String input;
        List<String> partOptions = Arrays.asList("0", "1", "2", "3", "4", "5");
        do {
            menuList.forEach(System.out::println);
            input = scanner.nextLine();
        } while (! partOptions.contains(input));
        menuActions.get(Integer.parseInt(input)).run();
    }

    public void create() {
        System.out.println("Enter ID");
        Long id = Long.parseLong(scanner.nextLine());
        System.out.println("Enter name");
        String name = scanner.nextLine();
        System.out.println("Enter type");
        String type = scanner.nextLine();
        System.out.println("Enter cost");
        double cost = Double.parseDouble(scanner.nextLine());
        Long repairId = Long.parseLong(scanner.nextLine());
        Part part = new Part(id, name, type, cost, repairId);
        partService.create(part);
    }

    public void delete() {
        System.out.println("Enter ID to delete");
        Long id = Long.parseLong(scanner.nextLine());
        partService.delete(id);
    }

    public void update() {
        System.out.println("Enter ID of an existing Part");
        Long id = Long.parseLong(scanner.nextLine());
        while (!partService.idExists(id)) {
            System.out.println("The ID you entered is invalid");
            System.out.println("Enter ID of an existing vehicle");
            id = Long.parseLong(scanner.nextLine());
        }
        System.out.println("Enter name");
        String name = scanner.nextLine();
        System.out.println("Enter type");
        String type = scanner.nextLine();
        System.out.println("Enter cost");
        double cost = Double.parseDouble(scanner.nextLine());
        Long repairId = Long.parseLong(scanner.nextLine());
        Part part = new Part(id, name, type, cost, repairId);
        partService.updateById(part);
    }

    public void findById() {
        System.out.println("Enter Part ID");
        Long id = Long.parseLong(scanner.nextLine());
        System.out.println(partService.findById(id));
    }

    public void findAll() {
        System.out.println(partService.findAll());
    }

    public void exit() {
        System.out.println("Going back to main");
        MainControllerNew mainControllerNew = new MainControllerNew();
        mainControllerNew.printMenu();
    }
}
