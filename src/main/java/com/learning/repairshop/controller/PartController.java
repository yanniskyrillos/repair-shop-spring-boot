package com.learning.repairshop.controller;

import com.learning.repairshop.domain.Part;
import com.learning.repairshop.service.PartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/parts")
public class PartController {

    @Autowired
    private PartService partService;

    @GetMapping
    public List<Part> findAll() {
        return partService.findAll();
    }

    @GetMapping(path = "/{id}")
    public Part findById(@PathVariable(value = "id") Long id) {
        return partService.findById(id);
    }

    @PostMapping
    public void create(@RequestBody Part part) {
        partService.create(part);
    }

    @PutMapping
    public void updateById(@RequestBody Part part) {
        partService.updateById(part);
    }

    @DeleteMapping(path = "/{id}/delete")
    public void delete(@PathVariable(value = "id") Long id) {
        partService.delete(id);
    }
}
