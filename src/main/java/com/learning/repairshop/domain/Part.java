package com.learning.repairshop.domain;

public class Part {
    private Long id;
    private String name;
    private String type;
    private double cost;
    private Long repairId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Long getRepairId() {
        return repairId;
    }

    public void setRepairId(Long repairId) {
        this.repairId = repairId;
    }

    @Override
    public String toString() {
        return "Part{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", cost=" + cost +
                ", repairId=" + repairId +
                '}';
    }

    public Part(Long id, String name, String type, double cost, Long repairId) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.cost = cost;
        this.repairId = repairId;
    }
}
