package com.learning.repairshop.domain;

import com.learning.repairshop.util.RepairStatus;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Repair {
    private Long id;
    private LocalDateTime dateTime;
    private RepairStatus status;
    private Long vehicleId;
    private double cost;
    private List<Part> listOfParts = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public RepairStatus getStatus() {
        return status;
    }

    public void setStatus(RepairStatus status) {
        this.status = status;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Part getPart(int partIndexInList) {
        return listOfParts.get(partIndexInList);
    }

    public void addPart(Part part) {
        listOfParts.add(part);
    }

    public int getNumberOfParts() {
        return listOfParts.size();
    }

    public void removePart(Part part) {
        listOfParts.remove(part);
    }

    public void updatePart(int index, Part part) {
        listOfParts.set(index, part);
    }

    public List<Part> getListOfParts() {
        return this.listOfParts;
    }

    public int getPartIndexInList(Part part) {
        return this.listOfParts.indexOf(part);
    }

    @Override
    public String toString() {
        return "Repair{" +
                "id=" + id +
                ", date=" + dateTime +
                ", status=" + status +
                ", vehicleId=" + vehicleId +
                ", cost=" + cost +
                ", listOfParts=" + listOfParts +
                '}';
    }

    public Repair(Long id, RepairStatus status, Long vehicleId, double cost, int year, int month, int day, int hour,
                  int minute) {
        this.id = id;
        this.status = status;
        this.vehicleId = vehicleId;
        this.cost = cost;
        this.dateTime = LocalDateTime.of(year, month, day, hour, minute);
    }
}
