package com.learning.repairshop;

import com.learning.repairshop.domain.Part;
import com.learning.repairshop.domain.Repair;
import com.learning.repairshop.domain.User;
import com.learning.repairshop.domain.Vehicle;
import com.learning.repairshop.persistence.*;
import com.learning.repairshop.service.RepairService;
import com.learning.repairshop.service.UserService;
import com.learning.repairshop.util.RepairStatus;
import java.sql.SQLException;

public class MainJdbc {


    public static void main(String[] args) throws SQLException {
        UserDao userDao = new UserDaoJdbc();
        User user = new User(4L, "luisenrique@barca.com", "viscailbarca", "Luis", "Enrique",
                "Catalunya 5", 45346, 1);
        //userDao.insert(user);
        //userDao.updateById(user);
        //System.out.println(userDao.findByEmail("leo@messi.com"));
        //userDao.findAll().forEach(System.out::println);
        //System.out.println(((UserDaoJdbc) userDao).idExists(1L));

        VehicleDaoJdbc vehicleDaoJdbc = new VehicleDaoJdbc();
        //vehicleDaoJdbc.findAllVehiclesByUser(1L).forEach(System.out::println);
        PartDao partDao = new PartDaoJdbc();
        //partDao.findAll().forEach(System.out::println);
        Part part = new Part(2L, "aPartName", "partType", 90.98, 1L);
        //partDao.updateById(part);
        //partDao.insert(part);
        //partDao.findAll().forEach(System.out::println);
        VehicleDao vehicleDao = new VehicleDaoJdbc();
        Vehicle vehicle = new Vehicle(4L, "WWW-0000", "NoBrand", "NoModel", "Color",
                33.22, 1L, 2000, 10, 10);
        //vehicleDao.insert(vehicle);
        //((VehicleDaoJdbc) vehicleDao).findAllVehiclesByUser(1L).forEach(System.out::println);
        Vehicle vehicle2 = new Vehicle(4L, "WWW-0420", "NoBrand", "NoModel", "Color",
                22.22, 1L, 2007, 10, 10);
        //vehicleDao.updateById(vehicle);
        //((VehicleDaoJdbc) vehicleDao).findAllVehiclesByUser(1L).forEach(System.out::println);
        Vehicle vehicle3 = new Vehicle(6L, "WWW-0320", "NoBrand", "NoModel", "Color",
                22.22, 10L, 2007, 10, 10);
        //vehicleDao.insert(vehicle3);


        //System.out.println(vehicleDao.findById(4L));
        //vehicleDao.updateById(vehicle);
        //System.out.println(vehicleDao.findById(4L));
        //vehicleDao.findAll().forEach(System.out::println);
        RepairDao repairDao = new RepairDaoJdbc();
        //repairDao.findAll().forEach(System.out::println);
        Repair repair = new Repair(1L, RepairStatus.PENDING, 1L, 333.22, 2007, 9, 9, 10,
                10);
        //repairDao.insert(repair);
        //repairDao.findAll().forEach(System.out::println);
        //repairDao.updateById(repair);
        //repairDao.findAll().forEach(System.out::println);

        //vehicleDao.updateById(vehicle2);
        //System.out.println(vehicleDao.findById(4L));

        PartDaoJdbc partDaoJdbc = new PartDaoJdbc();
        partDao.findAll().forEach(System.out::println);
        //System.out.println(((RepairDaoJdbc) repairDao).calculateRepairCost(1L));
        //System.out.println(partDaoJdbc.findById(2L));
        //System.out.println(repairDao.findById(1L));

        System.out.println(partDao.idExists(4L));
        RepairService repairService = new RepairService();
        System.out.println(repairDao.calculateRepairCost(1L));
        System.out.println(partDao.calculateCostOfAllPartsOfRepair(1L));
        //System.out.println(repairDao.calculateRepairCost(1L) + partDao.calculateCostOfAllPartsOfRepair(1L));
        //System.out.println(repairService.calculateTotalRepairCost(1L));
        UserService userService = new UserService();
        System.out.println(userService.findAllVehiclesByUser(1L));
    }
}
