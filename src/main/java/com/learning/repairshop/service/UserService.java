package com.learning.repairshop.service;

import com.learning.repairshop.domain.Repair;
import com.learning.repairshop.domain.User;
import com.learning.repairshop.domain.Vehicle;
import com.learning.repairshop.persistence.UserDao;
import com.learning.repairshop.persistence.UserDaoJdbc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private RepairService repairService;

    public boolean create(User user) {
        userDao.insert(user);
        return true;
    }

    public void delete(Long id) {
        userDao.delete(id);
    }

    public void updateById(User user) {
        userDao.updateById(user);
    }

    public User findByEmail(String email) throws NullPointerException {
        try {
            User user = userDao.findByEmail(email);
            List<Vehicle> vehicleList = vehicleService.findAllVehiclesByUser(user.getId());
            vehicleList.forEach(user::addVehicleToList);
            return user;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<User> findAll() {
        List<User> userList =  userDao.findAll();
        List<Vehicle> vehicleList = vehicleService.findAll();


        userList.forEach(user -> vehicleList.stream().filter(vehicle -> vehicle.getUserID().equals(user.getId()))
        .forEach(user::addVehicleToList));
        return userList;

        /*for(User user : userList) {
            for(Vehicle vehicle : vehicleList) {
                if (user.getId().equals(vehicle.getUserID())) {
                    user.addVehicleToList(vehicle);
                }
            }
        }
        return userList;*/
    }

    public User login(String email, String password) throws NullPointerException {
        return userDao.login(email, password);
    }

    public List<Vehicle> findAllVehiclesByUser(Long userId) {
        return vehicleService.findAllVehiclesByUser(userId);
    }

    public List<Repair> findAllRepairsOfAllVehiclesByUser(Long userId) {
        List<Repair> repairsOfAllVehiclesOfUser = new ArrayList<>();
        List<Vehicle> allVehiclesByUser = vehicleService.findAllVehiclesByUser(userId);
        for(Vehicle vehicle: allVehiclesByUser) {
            repairsOfAllVehiclesOfUser.addAll(repairService.findAllRepairsByVehicle(vehicle.getId()));
        }
        return repairsOfAllVehiclesOfUser;
    }

    public boolean idExists(Long id) {
        try {
            return userDao.idExists(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
