package com.learning.repairshop.service;

import com.learning.repairshop.domain.Part;
import com.learning.repairshop.persistence.PartDao;
import com.learning.repairshop.persistence.PartDaoJdbc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class PartService {

    @Autowired
    private PartDao partDao;

    public void create(Part part) {
        partDao.insert(part);
    }

    public List<Part> findAll() {
        return partDao.findAll();
    }

    public void delete(Long id) {
        partDao.delete(id);
    }

    public void updateById(Part part) {
        partDao.updateById(part);
    }

    public Part findById(Long id) {
        try {
            return partDao.findById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public double calculateCostOfAllPartsOfRepair(Long id) {
        try {
            return partDao.calculateCostOfAllPartsOfRepair(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean idExists(Long id) {
        try {
            return partDao.idExists(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<Part> findAllByRepair(Long repairId) {
        return partDao.findAllByRepair(repairId);
    }
}
