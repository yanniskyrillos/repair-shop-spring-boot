package com.learning.repairshop.service;

import com.learning.repairshop.domain.Repair;
import com.learning.repairshop.domain.Vehicle;
import com.learning.repairshop.persistence.VehicleDaoList;

import java.util.List;

public class VehicleServiceListImpl {

    private static VehicleServiceListImpl vehicleService;
    private VehicleServiceListImpl() {}
    public static VehicleServiceListImpl getInstance() {
        if(vehicleService==null) {
            vehicleService = new VehicleServiceListImpl();
        }
        return vehicleService;
    }

    VehicleDaoList vehicleDAO = VehicleDaoList.getInstance();
    UserServiceListImpl userService = UserServiceListImpl.getInstance();
    RepairServiceListImpl repairService = RepairServiceListImpl.getInstance();


    public boolean create(Vehicle vehicle) {
        boolean userExists = userService.checkForId(vehicle.getUserID());
        if (userExists) {
            vehicleDAO.insert(vehicle);
            userService.addVehicleToUser(vehicle);
            return true;
        }
        return false;
    }
    public List<Vehicle> findAll() {
        return vehicleDAO.findAll();
    }
    public Vehicle findById(Long id) {
        return vehicleDAO.findById(id);
    }
    public void updateById(Vehicle vehicle) {
        vehicleDAO.updateById(vehicle);
        userService.updateVehicleInUserList(vehicle);
    }
    public void deleteById(Long id) {
        vehicleDAO.delete(id);
    }
    public List<Repair> findAllRepairsByVehicle(Long vehicleId) {
        return repairService.findAllRepairsByVehicle(vehicleId);
    }

    public boolean checkForId(Long id) {
        return vehicleDAO.checkForId(id);
    }
}
