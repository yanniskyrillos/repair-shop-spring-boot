package com.learning.repairshop.service;

import com.learning.repairshop.domain.Part;
import com.learning.repairshop.persistence.PartDaoList;

import java.util.List;

public class PartServiceListImpl {

    private static PartServiceListImpl partService;
    private PartServiceListImpl() {}
    public static PartServiceListImpl getInstance() {
        if(partService==null) {
            partService = new PartServiceListImpl();
        }
        return partService;
    }

    RepairServiceListImpl repairService = RepairServiceListImpl.getInstance();
    PartDaoList partDAO = PartDaoList.getInstance();

    public boolean create(Part part) {
        boolean repairExists = repairService.checkForId(part.getRepairId());
        if (repairExists) {
            partDAO.insert(part);
            repairService.addPartToRepair(part);
            return true;
        }
        else {
            return false;
        }
    }
    public List<Part> findAll() {
        return partDAO.findAll();
    }
    public void delete(Long id) {
        Part partToRemove = partDAO.delete(id);
        repairService.removePartFromList(partToRemove);
    }
    public void updateById(Part partUpdated) {
        partDAO.updateById(partUpdated);
        repairService.updatePartRepairList(partUpdated);
    }
    public Part findById(Long idToFind) {
        return partDAO.findById(idToFind);
    }
    public boolean checkForId(Long id) {
        return partDAO.checkForId(id);
    }
}
