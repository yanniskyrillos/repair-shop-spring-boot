package com.learning.repairshop.service;

import com.learning.repairshop.domain.Vehicle;
import com.learning.repairshop.persistence.VehicleDao;
import com.learning.repairshop.persistence.VehicleDaoJdbc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class VehicleService {

    @Autowired
    private VehicleDao vehicleDao;

    public void create(Vehicle vehicle) {
        vehicleDao.insert(vehicle);
    }

    public List<Vehicle> findAll() {
        return vehicleDao.findAll();
    }

    public Vehicle findById(Long id) {
        try {
            return vehicleDao.findById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateById(Vehicle vehicle) {
        vehicleDao.updateById(vehicle);
    }

    public void delete(Long id) {
        vehicleDao.delete(id);
    }

    public List<Vehicle> findAllVehiclesByUser(Long userId) {
        return vehicleDao.findAllVehiclesByUser(userId);
    }

    public boolean idExists(Long id) {
        try {
            return vehicleDao.idExists(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
