package com.learning.repairshop.service;

import com.learning.repairshop.domain.Part;
import com.learning.repairshop.domain.Repair;
import com.learning.repairshop.persistence.RepairDao;
import com.learning.repairshop.persistence.RepairDaoJdbc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import java.sql.SQLException;
import java.util.List;

@Service
public class RepairService {

    @Autowired
    private RepairDao repairDao;

    @Autowired
    PartService partService;

    public void create(Repair repair) {
        repairDao.insert(repair);
    }

    public List<Repair> findAll() {
        List<Repair> repairList =  repairDao.findAll();
        List<Part> partList = partService.findAll();
        repairList.forEach(repair -> partList.stream().filter(part -> part.getRepairId().equals(repair.getId()))
        .forEach(repair::addPart));
        return repairList;
    }

    public Repair findById(Long id) {
        try {
            Repair repair = repairDao.findById(id);
            List<Part> partList = partService.findAllByRepair(id);
            partList.forEach(repair::addPart);
            return repair;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateById(Repair repair) {
        repairDao.updateById(repair);
    }

    public void delete(Long id) {
        repairDao.delete(id);
    }

    public double calculateTotalRepairCost(Long id) {
        try {
            return repairDao.calculateRepairCost(id) + partService.calculateCostOfAllPartsOfRepair(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Repair> findAllRepairsByVehicle(Long vehicleId) {
        try {
            return repairDao.findAllRepairsByVehicle(vehicleId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean idExists(Long id) {
        try {
            return repairDao.idExists(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
