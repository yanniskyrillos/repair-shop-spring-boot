package com.learning.repairshop.service;

import com.learning.repairshop.domain.Part;
import com.learning.repairshop.domain.Repair;
import com.learning.repairshop.persistence.RepairDaoList;

import java.util.List;

public class RepairServiceListImpl {

    public static RepairServiceListImpl repairService;
    private RepairServiceListImpl() {}
    public static RepairServiceListImpl getInstance() {
        if(repairService==null) {
            repairService = new RepairServiceListImpl();
        }
        return repairService;
    }

    RepairDaoList repairDAO = RepairDaoList.getInstance();

    static VehicleServiceListImpl vehicleService = VehicleServiceListImpl.getInstance();
    //VehicleServiceListImpl vehicleService;

    public boolean create(Repair repair) {
        boolean vehicleExists = vehicleService.checkForId(repair.getVehicleId());
        if(vehicleExists) {
            repairDAO.insert(repair);
            return true;
        }
        else {
            return false;
        }
    }
    public List<Repair> findAll() {
        return repairDAO.findAll();
    }
    public Repair findById(Long id) {
        return repairDAO.findById(id);
    }
    public void updateById(Repair repair) {
        repairDAO.updateById(repair);
    }
    public void deleteById(Long id) {
        repairDAO.delete(id);
    }
    public double calculateTotalRepairCost(Long id) {
        return repairDAO.calculateCostOfRepairAndParts(id);
    }

    public void removePartFromList(Part partToRemove) {
        repairDAO.removePart(partToRemove);
    }

    public List<Repair> findAllRepairsByVehicle(Long vehicleId) {
        return repairDAO.findAllRepairsByVehicle(vehicleId);
    }

    public void addPartToRepair(Part part) {
        repairDAO.addPartToRepair(part);
    }

    public void updatePartRepairList(Part part) {
        repairDAO.updatePartRepairList(part);
    }

    public boolean checkForId(Long id) {
        return repairDAO.checkForId(id);
    }
}
