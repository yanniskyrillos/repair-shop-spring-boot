package com.learning.repairshop.service;

import com.learning.repairshop.domain.Repair;
import com.learning.repairshop.domain.User;
import com.learning.repairshop.domain.Vehicle;
import com.learning.repairshop.persistence.UserDaoList;

import java.util.ArrayList;
import java.util.List;

public class UserServiceListImpl {

    private static UserServiceListImpl userService;
    private UserServiceListImpl() {}
    public static UserServiceListImpl getInstance() {
        if(userService == null) {
            userService = new UserServiceListImpl();
        }
        return userService;
    }

    UserDaoList userDAO = UserDaoList.getInstance();
    RepairServiceListImpl repairService = RepairServiceListImpl.getInstance();

    public boolean create(User user) {
        boolean userIdAlreadyInUse = userDAO.checkForId(user.getId());
        if (userIdAlreadyInUse) {
            return false;
        }
        userDAO.insert(user);
        return true;
    }

    public void delete(Long id) {
        userDAO.delete(id);
    }

    public void updateById(User user) {
        userDAO.updateById(user);
    }

    public User findByEmail(String email) throws NullPointerException {
        return userDAO.findByEmail(email);
    }

    public List<User> findAll() {
        return userDAO.findAll();
    }

    public User login(String email, String password) throws NullPointerException {
        return userDAO.login(email, password);
    }

    public List<Vehicle> findAllVehiclesByUser(Long userId) {
        return userDAO.findAllVehiclesByUser(userId);
    }

    public List<Repair> findAllRepairsOfAllVehiclesByUser(Long userId) {
        List<Repair> repairsOfAllVehiclesOfUser = new ArrayList<>();
        List<Vehicle> allVehiclesByUser = userDAO.findAllVehiclesByUser(userId);
        for(Vehicle vehicle: allVehiclesByUser) {
            repairsOfAllVehiclesOfUser.addAll(repairService.findAllRepairsByVehicle(vehicle.getId()));
        }
        return repairsOfAllVehiclesOfUser;
    }

    public void addVehicleToUser(Vehicle vehicle) {
        userDAO.addVehicleToUser(vehicle);
    }

    public boolean checkForId(Long id) {
        return userDAO.checkForId(id);
    }

    public void updateVehicleInUserList(Vehicle vehicle) {
        userDAO.updateVehicleInUserList(vehicle);
    }
}
