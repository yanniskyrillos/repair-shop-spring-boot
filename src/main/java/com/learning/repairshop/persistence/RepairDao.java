package com.learning.repairshop.persistence;

import com.learning.repairshop.domain.Repair;

import java.sql.SQLException;
import java.util.List;

public interface RepairDao {
    void insert(Repair repair);
    List<Repair> findAll();
    Repair findById(Long id) throws SQLException;
    void updateById(Repair repair);
    void delete(Long id);
    double calculateRepairCost(Long id) throws SQLException;
    List<Repair> findAllRepairsByVehicle(Long vehicleId) throws SQLException;
    boolean idExists(Long id) throws SQLException;
}
