package com.learning.repairshop.persistence;

import com.learning.repairshop.domain.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class VehicleDaoList {

    private List<Vehicle> vehicleList = new ArrayList<>();

    private static VehicleDaoList vehicleDAO;
    private VehicleDaoList() {}
    public static VehicleDaoList getInstance() {
        if (vehicleDAO==null) {
            vehicleDAO = new VehicleDaoList();
        }
        return vehicleDAO;
    }

    public void insert(Vehicle vehicle) {
        vehicleList.add(vehicle);
    }

    public List<Vehicle> findAll() {
        return vehicleList;
    }

    public Vehicle findById(Long id) {
        return vehicleList
                .stream()
                .filter(vehicle -> vehicle.getId().equals(id))
                .findFirst().orElse(null);
    }

    public void updateById(Vehicle vehicleUpdated) {
        Vehicle vehicleOld =
                vehicleList.stream()
                            .filter(vehicle -> vehicle.getId().equals(vehicleUpdated.getId()))
                            .findFirst()
                            .orElseThrow(RuntimeException::new);

        int index = vehicleList.indexOf(vehicleOld);
        vehicleList.set(index, vehicleUpdated);
    }

    public void delete(Long id) {
        Vehicle vehicleToDelete = vehicleList.stream()
                                .filter(vehicle -> vehicle.getId().equals(id))
                                .findFirst()
                                .orElseThrow(RuntimeException::new);
        vehicleList.remove(vehicleToDelete);
    }

    public boolean checkForId(Long id) {
        return vehicleList
                .stream()
                .anyMatch(vehicle -> vehicle.getId().equals(id));
    }
}
