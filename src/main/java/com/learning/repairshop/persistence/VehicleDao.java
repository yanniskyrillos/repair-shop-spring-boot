package com.learning.repairshop.persistence;

import com.learning.repairshop.domain.Vehicle;

import java.sql.SQLException;
import java.util.List;

public interface VehicleDao {
    void insert(Vehicle vehicle);
    List<Vehicle> findAll();
    Vehicle findById(Long id) throws SQLException;
    void updateById(Vehicle vehicle);
    void delete(Long id);
    List<Vehicle> findAllVehiclesByUser(Long userId);
    boolean idExists(Long id) throws SQLException;
}
