package com.learning.repairshop.persistence;

import com.learning.repairshop.domain.Vehicle;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Repository
public class VehicleDaoJdbc implements VehicleDao {

    final static String url = "jdbc:postgresql://localhost/vehicle_repair_shop";
    final static String userName = "postgres";
    final static String password = "postgres";

    @Override
    public void insert(Vehicle vehicle) {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement = connection.prepareStatement("insert into public.vehicle " +
                        "(plate_number, brand, model, creation_date, color, price, user_id)" +
                        "values (?, ?, ?, ?, ?, ?, ?)")
        )
        {
            preparedStatement.setString(1, vehicle.getPlateNumber());
            preparedStatement.setString(2, vehicle.getBrand());
            preparedStatement.setString(3, vehicle.getModel());
            preparedStatement.setDate(4, Date.valueOf(vehicle.getCreationDate()));
            preparedStatement.setString(5, vehicle.getColor());
            preparedStatement.setDouble(6, vehicle.getPrice());
            preparedStatement.setLong(7, vehicle.getUserID());
            preparedStatement.execute();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Vehicle> findAll() {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select id, plate_number, brand," +
                        "model, creation_date, color, price, user_id from public.vehicle")
        )
        {
            List<Vehicle> listOfVehicles = new ArrayList<>();
            while (resultSet.next()) {
                Long id = resultSet.getLong(1);
                String plateNumber = resultSet.getString(2);
                String brand = resultSet.getString(3);
                String model = resultSet.getString(4);
                LocalDate creationDate = resultSet.getDate(5).toLocalDate();
                String color = resultSet.getString(6);
                double price = resultSet.getDouble(7);
                Long userId = resultSet.getLong(8);
                Vehicle vehicle = new Vehicle(id, plateNumber, brand, model, color, price, userId, creationDate.getYear(),
                        creationDate.getMonthValue(), creationDate.getDayOfMonth());
                listOfVehicles.add(vehicle);
            }
            return listOfVehicles;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Vehicle findById(Long id) throws SQLException {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("select plate_number, brand," +
                                "model, creation_date, color, price, user_id from public.vehicle where id = ?")
        )
        {
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();


            if (resultSet.next()) {
                String plateNumber = resultSet.getString(1);
                String brand = resultSet.getString(2);
                String mobel= resultSet.getString(3);
                LocalDate creationDate = resultSet.getDate(4).toLocalDate();
                String color = resultSet.getString(5);
                double price = resultSet.getDouble(6);
                Long userId = resultSet.getLong(7);
                Vehicle vehicle = new Vehicle(id, plateNumber, brand, mobel, color, price, userId, creationDate.getYear(),
                        creationDate.getMonthValue(), creationDate.getDayOfMonth());
                return vehicle;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return null;
    }

    @Override
    public void updateById(Vehicle vehicle) {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("update public.vehicle " +
                                "set plate_number = ?, brand = ?, model = ?, creation_date = ?," +
                                " color = ?, price = ?, user_id = ? " +
                                "where id = ?")
        )
        {
            preparedStatement.setString(1, vehicle.getPlateNumber());
            preparedStatement.setString(2, vehicle.getBrand());
            preparedStatement.setString(3, vehicle.getModel());
            preparedStatement.setDate(4, Date.valueOf(vehicle.getCreationDate()));
            preparedStatement.setString(5, vehicle.getColor());
            preparedStatement.setDouble(6, vehicle.getPrice());
            preparedStatement.setLong(7, vehicle.getUserID());
            preparedStatement.setLong(8, vehicle.getId());
            preparedStatement.execute();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Long id) {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("delete from public.vehicle where id = ?")
        )
        {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Vehicle> findAllVehiclesByUser(Long userId) {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =connection.prepareStatement("select id, plate_number, brand," +
                        "model, creation_date, color, price from public.vehicle where user_id = ?")
        )
        {
            List<Vehicle> listOfVehicles = new ArrayList<>();
            preparedStatement.setLong(1, userId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Long id = resultSet.getLong(1);
                String plateNumber = resultSet.getString(2);
                String brand = resultSet.getString(3);
                String model = resultSet.getString(4);
                LocalDate creationDate = resultSet.getDate(5).toLocalDate();
                String color = resultSet.getString(6);
                double price = resultSet.getDouble(7);
                Vehicle vehicle = new Vehicle(id, plateNumber, brand, model, color, price, userId, creationDate.getYear(),
                        creationDate.getMonthValue(), creationDate.getDayOfMonth());
                listOfVehicles.add(vehicle);
            }
            return listOfVehicles;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean idExists(Long id) throws SQLException {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("select count(plate_number) from public.vehicle where id = ?")
        )
        {
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1) != 0;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return false;
    }
}
