package com.learning.repairshop.persistence;

import com.learning.repairshop.domain.Part;
import com.learning.repairshop.domain.Repair;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RepairDaoList {

    private List<Repair> repairList = new ArrayList<>();

    private static RepairDaoList repairDAO;
    private RepairDaoList() {}
    public static RepairDaoList getInstance() {
        if (repairDAO == null) {
            repairDAO = new RepairDaoList();
        }
        return repairDAO;
    }

    public List<Repair> findAllRepairsByVehicle(Long vehicleId) {
        return repairList.stream()
                .filter(repair -> repair.getVehicleId().equals(vehicleId))
                .collect(Collectors.toList());
    }

    public void insert(Repair repair) {
        repairList.add(repair);
    }

    public List<Repair> findAll() {
        return repairList;
    }

    public Repair findById(Long id) {
        return repairList
                .stream()
                .filter(repair -> repair.getId().equals(id))
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }

    public void updateById(Repair repairUpdated) {
        Repair repairOld =
                repairList.stream()
                          .filter(repair -> repair.getId().equals(repairUpdated.getId()))
                          .findFirst()
                          .orElseThrow(RuntimeException::new);
        int index = repairList.indexOf(repairOld);
        repairList.set(index, repairUpdated);
    }

    public void delete(Long id) {
        Repair repairToDelete =
                repairList.stream()
                          .filter(repair -> repair.getId().equals(id))
                          .findFirst()
                          .orElseThrow(RuntimeException::new);
        repairList.remove(repairToDelete);
    }

    public double calculateCostOfRepairAndParts(Long id) {
        double cost = 0.0;
        Repair repair = repairList.stream()
                .filter(repair1 -> repair1.getId().equals(id))
                .findFirst()
                .orElseThrow(RuntimeException::new);

        //for loop could be replaced with a stream if repair.listOfParts gets public access
        for (int i = 0; i<repair.getNumberOfParts(); i++) {
            cost = cost + repair.getPart(i).getCost();
        }
        return cost;
    }

    public void removePart(Part partToRemove) {
        Repair repair =
                repairList.stream()
                          .filter(repair1 -> repair1.getId().equals(partToRemove.getRepairId()))
                          .findFirst()
                          .orElseThrow(RuntimeException::new);
        repair.getListOfParts().remove(partToRemove);
    }

    public void addPartToRepair(Part part) {
        Repair repair = repairList
                        .stream()
                        .filter(repair1 -> repair1.getId().equals(part.getRepairId()))
                        .findFirst()
                        .orElseThrow(RuntimeException::new);
        repair.addPart(part);
    }

    public void updatePartRepairList(Part part) {
        Repair repair =
                repairList.stream()
                        .filter(repair1 -> repair1.getId().equals(part.getRepairId()))
                        .findFirst()
                        .orElseThrow(RuntimeException::new);

        Part partOld = repair.getListOfParts()
                        .stream()
                        .filter(part1 -> part1.getId().equals(part.getId()))
                        .findFirst()
                        .orElseThrow(RuntimeException::new);
        int i = repair.getPartIndexInList(partOld);
        repair.updatePart(i, part);
    }

    public boolean checkForId(Long id) {
        return repairList.stream()
                        .anyMatch(repair -> repair.getId().equals(id));
    }

}
