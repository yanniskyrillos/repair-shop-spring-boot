package com.learning.repairshop.persistence;

import com.learning.repairshop.domain.Part;

import java.sql.SQLException;
import java.util.List;

public interface PartDao {
    void insert(Part part);
    List<Part> findAll();
    Part delete(Long id);
    void updateById(Part part);
    Part findById(Long id) throws SQLException;
    double calculateCostOfAllPartsOfRepair(Long id) throws SQLException;
    boolean idExists(Long id) throws SQLException;
    List<Part> findAllByRepair(Long repairId);
}
