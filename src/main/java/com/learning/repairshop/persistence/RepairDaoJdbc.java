package com.learning.repairshop.persistence;

import com.learning.repairshop.domain.Repair;
import com.learning.repairshop.util.RepairStatus;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RepairDaoJdbc implements RepairDao {

    final static String url = "jdbc:postgresql://localhost/vehicle_repair_shop";
    final static String userName = "postgres";
    final static String password = "postgres";

    @Override
    public void insert(Repair repair) {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement = connection.prepareStatement("insert into public.repair " +
                        "(date_time, status, vehicle_id, cost)" +
                        "values (?, ?, ?, ?)")
        )
        {
            preparedStatement.setTimestamp(1, Timestamp.valueOf(repair.getDateTime()));
            preparedStatement.setString(2, repair.getStatus().toString());
            preparedStatement.setInt(3, repair.getVehicleId().intValue());
            preparedStatement.setDouble(4, repair.getCost());
            preparedStatement.execute();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Repair> findAll() {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select id, date_time, status," +
                        "vehicle_id, cost from public.repair")
        )
        {
            List<Repair> listOfRepairs = new ArrayList<>();
            while (resultSet.next()) {
                Long id = (long) resultSet.getInt(1);
                LocalDateTime dateTime = resultSet.getTimestamp(2).toLocalDateTime();
                RepairStatus repairStatus = RepairStatus.valueOf(resultSet.getString(3));
                Long vehicleId = resultSet.getLong(4);
                double cost = resultSet.getDouble(5);
                Repair repair = new Repair(id, repairStatus, vehicleId, cost, dateTime.getYear(), dateTime.getMonthValue(),
                        dateTime.getDayOfMonth(), dateTime.getHour(), dateTime.getMinute());
                listOfRepairs.add(repair);
            }
            return listOfRepairs;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Repair findById(Long id) throws SQLException {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement = connection.prepareStatement("select date_time, status," +
                        "vehicle_id, cost from public.repair where id = ?")
        )
        {
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                LocalDateTime dateTime = resultSet.getTimestamp(1).toLocalDateTime();
                RepairStatus repairStatus = RepairStatus.valueOf(resultSet.getString(2));
                Long vehicleId = resultSet.getLong(3);
                double cost = resultSet.getDouble(4);
                Repair repair = new Repair(id, repairStatus, vehicleId, cost,
                        dateTime.getYear(), dateTime.getMonthValue(), dateTime.getDayOfMonth(), dateTime.getHour(),
                        dateTime.getMinute());
                return repair;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return null;
    }

    @Override
    public void updateById(Repair repair) {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("update public.repair " +
                                "set date_time = ?, status = ?, vehicle_id = ?, cost = ? " +
                                "where id = ?")
        )
        {
            preparedStatement.setTimestamp(1, Timestamp.valueOf(repair.getDateTime()));
            preparedStatement.setString(2, repair.getStatus().toString());
            preparedStatement.setInt(3, repair.getVehicleId().intValue());
            preparedStatement.setDouble(4, repair.getCost());
            preparedStatement.setLong(5, repair.getId());
            preparedStatement.execute();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Long id) {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("delete from public.repair where id = ?")
        )
        {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public double calculateRepairCost(Long id) throws SQLException {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("select cost " +
                                "from public.repair " +
                                "where id = ?")
                )
        {
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getDouble(1);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return 0;
    }

    public List<Repair> findAllRepairsByVehicle(Long vehicleId) throws SQLException {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("select id, date_time, status, cost from public.repair " +
                                "where vehicle_id = ?")
                )
        {
            preparedStatement.setLong(1, vehicleId);
            resultSet = preparedStatement.executeQuery();
            List<Repair> allRepairsOfVehicle = new ArrayList<>();
            while (resultSet.next()) {
                Long id = (long) resultSet.getInt(1);
                LocalDateTime dateTime = resultSet.getTimestamp(2).toLocalDateTime();
                RepairStatus repairStatus = RepairStatus.valueOf(resultSet.getString(3));
                double cost = resultSet.getDouble(4);
                Repair repair = new Repair(id, repairStatus, vehicleId, cost, dateTime.getYear(), dateTime.getMonthValue(),
                        dateTime.getDayOfMonth(), dateTime.getHour(), dateTime.getMinute());
                allRepairsOfVehicle.add(repair);
            }
            return allRepairsOfVehicle;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return null;
    }

    public boolean idExists(Long id) throws SQLException {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("select count(vehicle_id) from public.repair where id = ?")
        )
        {
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1) != 0;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return false;
    }
}
