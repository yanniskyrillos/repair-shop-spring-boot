package com.learning.repairshop.persistence;

import com.learning.repairshop.domain.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDao {
    void insert(User user);
    User findByEmail(String email) throws SQLException;
    void delete(Long id);
    void updateById(User user);
    List<User> findAll();
    User login(String email, String password);
    boolean idExists(Long id) throws SQLException;
}
