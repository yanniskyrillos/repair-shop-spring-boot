package com.learning.repairshop.persistence;

import com.learning.repairshop.domain.User;
import com.learning.repairshop.domain.Vehicle;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

public class UserDaoList {
    private int id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String address;
    private ArrayList<Vehicle> listOfVehicles;
    private int afm;
    private String type;

    private List<User> userList = new ArrayList<>();

    private static UserDaoList userDAO;
    private UserDaoList() {}
    public static UserDaoList getInstance() {
        if(userDAO==null) {
            userDAO = new UserDaoList();
        }
        return userDAO;
    }

    public void insert(User user) {
        userList.add(user);
    }

    public void delete(Long idToDelete) {
        User userToDelete = userList.stream()
                .filter(user -> user.getId().equals(idToDelete))
                .findFirst()
                .orElseThrow(RuntimeException::new);
        userList.remove(userToDelete);
    }

    public void updateById(User userUpdated) {
        User userOld =
                userList.stream()
                .filter(user -> user.getId().equals(userUpdated.getId()))
                .findFirst()
                .orElseThrow(RuntimeException::new);
        int index = userList.indexOf(userOld);
        userList.set(index, userUpdated);
    }

    public User findByEmail(String email) throws NullPointerException {
        return userList
                .stream()
                .filter(user -> user.getEmail().equals(email))
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }

    public List<User> findAll() {
        return userList;
    }

    public User login( String email, String password) throws NullPointerException {
        return userList.stream()
                            .filter(user1 -> user1.getEmail().equals(email) && user1.getPassword().equals(password))
                            .findFirst()
                            .orElseThrow(RuntimeException::new);
    }

    public List<Vehicle> findAllVehiclesByUser(Long userId) {
         return userList
                .stream()
                .filter(user -> user.getId().equals(userId))
                .findFirst()
                .orElseThrow(RuntimeException::new)
                .getVehicleList();
    }

    public void addVehicleToUser(Vehicle vehicle) {
        User user = userList.stream()
                            .filter(user1 -> user1.getId().equals(vehicle.getUserID()))
                            .findFirst()
                            .orElseThrow(RuntimeException::new);
        user.addVehicleToList(vehicle);
    }

    public boolean checkForId(Long id) {
        return userList.stream()
                .anyMatch(user -> user.getId().equals(id));
    }

    public void updateVehicleInUserList(Vehicle vehicle) {
        User user = userList.stream()
                .filter(user1 -> user1.getId().equals(vehicle.getUserID()))
                .findFirst()
                .orElseThrow(RuntimeException::new);

        Vehicle vehicleOld = user
                .getVehicleList()
                .stream()
                .filter(vehicle1 -> vehicle1.getId().equals(vehicle.getId()))
                .findFirst()
                .orElseThrow(RuntimeException::new);

        int i = user.getVehicleIndexInList(vehicleOld);
        user.updateVehicle(i, vehicle);
    }
}
