package com.learning.repairshop.persistence;

import com.learning.repairshop.domain.User;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDaoJdbc implements UserDao {

    final static String url = "jdbc:postgresql://localhost/vehicle_repair_shop";
    final static String userName = "postgres";
    final static String password = "postgres";

    @Override
    public void insert(User user) {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement = connection.prepareStatement("insert into public.\"user\" " +
                        "(email, password, first_name, last_name, address, afm, type_id)" +
                        "values (?, ?, ?, ?, ?, ?, ?)")
        )
        {
            preparedStatement.setString(1, user.getEmail());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getFirstName());
            preparedStatement.setString(4, user.getLastName());
            preparedStatement.setString(5, user.getAddress());
            preparedStatement.setInt(6, user.getAfm());
            preparedStatement.setInt(7, user.getType());
            preparedStatement.execute();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User findByEmail(String email) throws SQLException {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("select first_name, last_name," +
                                "email, password, address, afm, id, type_id from public.\"user\" where email = ?")
        )
        {
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();


            if (resultSet.next()) {
                String firstName = resultSet.getString(1);
                String lastName = resultSet.getString(2);
                String emailFound = resultSet.getString(3);
                String password = resultSet.getString(4);
                String address = resultSet.getString(5);
                int afm = resultSet.getInt(6);
                Long id = resultSet.getLong(7);
                int type = resultSet.getInt(8);
                User user = new User(id, emailFound, password, firstName, lastName, address, afm, type);
                return user;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return null;
    }

    @Override
    public void delete(Long id) {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("delete from public.\"user\" where id = ?")
        )
        {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
        }
        catch (SQLException e) {
                    e.printStackTrace();
        }
    }

    @Override
    public void updateById(User user) {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("update public.\"user\"" +
                                "set first_name = ?, last_name = ?, email = ?, password = ?, address = ?, afm = ?," +
                                "type_id = ? where id = ?")
        )
        {
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getAddress());
            preparedStatement.setInt(6, user.getAfm());
            preparedStatement.setInt(7, user.getType());
            preparedStatement.setLong(8, user.getId());
            preparedStatement.execute();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> findAll() {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select first_name, last_name," +
                        "email, password, address, afm, id, type_id from public.\"user\"")
        )
        {
            List<User> listOfUsers = new ArrayList<>();
            while (resultSet.next()) {
                String firstName = resultSet.getString(1);
                String lastName = resultSet.getString(2);
                String email = resultSet.getString(3);
                String password = resultSet.getString(4);
                String address = resultSet.getString(5);
                int afm = resultSet.getInt(6);
                Long id = resultSet.getLong(7);
                int type = resultSet.getInt(8);
                User user = new User(id, email, password, firstName, lastName, address, afm, type);
                listOfUsers.add(user);
            }
            return listOfUsers;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User login(String email, String password) {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("select first_name, last_name" +
                                "address, afm, id, type_id from public.\"user\" where email = ? and \"password\" = ?")
                )
        {
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String firstName = resultSet.getString(1);
                String lastName = resultSet.getString(2);
                String address = resultSet.getString(3);
                int afm = resultSet.getInt(4);
                Long id = resultSet.getLong(5);
                int type = resultSet.getInt(6);
                User user = new User(id, email, password, firstName, lastName, address, afm, type);
                return user;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean idExists(Long id) throws SQLException {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("select count(first_name) from public.\"user\" where id = ?")
        )
        {
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1) != 0;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return false;
    }
}
