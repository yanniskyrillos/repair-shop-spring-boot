package com.learning.repairshop.persistence;

import com.learning.repairshop.domain.Part;

import java.util.ArrayList;
import java.util.List;

public class PartDaoList {

    private List<Part> partList = new ArrayList<>();

    private static PartDaoList partDAO;
    private PartDaoList() {}
    public static PartDaoList getInstance() {
        if (partDAO == null) {
            partDAO = new PartDaoList();
        }
        return partDAO;
    }


    public void insert(Part part) {
        partList.add(part);
    }

    public List<Part> findAll() {
        return partList;
    }

    public Part delete(Long id) {
        Part partToDelete =
                partList.stream()
                        .filter(part -> part.getId().equals(id))
                        .findFirst()
                        .orElseThrow(RuntimeException::new);
        partList.remove(partToDelete);
        return partToDelete;
    }

    public void updateById(Part partUpdated) {
        Part partOld =
                partList.stream()
                        .filter(part -> part.getId().equals(partUpdated.getId()))
                        .findFirst()
                        .orElseThrow(RuntimeException::new);
        int index = partList.indexOf(partOld);
        partList.set(index, partUpdated);
    }

    public Part findById(Long idToFind) {
        return partList
                .stream()
                .filter(part -> part.getId().equals(idToFind))
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }

    public boolean checkForId(Long id) {
        return partList.stream()
                .anyMatch(part -> part.getId().equals(id));
    }
}
