package com.learning.repairshop.persistence;

import com.learning.repairshop.domain.Part;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PartDaoJdbc implements PartDao {

    final static String url = "jdbc:postgresql://localhost/vehicle_repair_shop";
    final static String userName = "postgres";
    final static String password = "postgres";

    @Override
    public void insert(Part part) {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement = connection.prepareStatement("insert into public.part " +
                        "(name, type, cost, repair_id)" +
                        "values (?, ?, ?, ?)")
        ) {
            preparedStatement.setString(1, part.getName());
            preparedStatement.setString(2, part.getType());
            preparedStatement.setDouble(3, part.getCost());
            preparedStatement.setLong(4, part.getRepairId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Part> findAll() {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select id, name, type, " +
                        " cost, repair_id from public.part")
        ) {
            List<Part> listOfParts = new ArrayList<>();
            while (resultSet.next()) {
                Long id = resultSet.getLong(1);
                String name = resultSet.getString(2);
                String type = resultSet.getString(3);
                double cost = resultSet.getDouble(4);
                Long repairId = resultSet.getLong(5);
                Part part = new Part(id, name, type, cost, repairId);
                listOfParts.add(part);
            }
            return listOfParts;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Part delete(Long id) {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("delete from public.part where id = ?")
        ) {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateById(Part part) {
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("update public.part " +
                                "set name = ?, type = ?, cost = ?, repair_id = ? " +
                                "where id = ?")
        ) {
            preparedStatement.setString(1, part.getName());
            preparedStatement.setString(2, part.getType());
            preparedStatement.setDouble(3, part.getCost());
            preparedStatement.setLong(4, part.getId());
            preparedStatement.setLong(5, part.getRepairId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Part findById(Long id) throws SQLException {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement = connection.prepareStatement("select name, type, " +
                        "cost, repair_id from public.part where id = ?")
        ) {
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String name = resultSet.getString(1);
                String type = resultSet.getString(2);
                double cost = resultSet.getDouble(3);
                Long repairId = resultSet.getLong(4);
                Part part = new Part(id, name, type, cost, repairId);
                return part;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return null;
    }

    public double calculateCostOfAllPartsOfRepair(Long id) throws SQLException {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("select sum(public.part.cost) " +
                                "from public.part " +
                                "where repair_id = ?")
        ) {
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getDouble(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return 0;
    }

    public boolean idExists(Long id) throws SQLException {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement =
                        connection.prepareStatement("select count(id) from public.part where id = ?")
        ) {
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1) != 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return false;
    }

    public List<Part> findAllByRepair(Long repairId) {
        ResultSet resultSet = null;
        try (
                Connection connection = DriverManager.getConnection(url, userName, password);
                PreparedStatement preparedStatement = connection.prepareStatement("select id, name, type, " +
                        "cost from public.part where repair_id = ?")
        ) {
            List<Part> partList = new ArrayList<>();
            preparedStatement.setLong(1, repairId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Long id = resultSet.getLong(1);
                String name = resultSet.getString(2);
                String type = resultSet.getString(3);
                double cost = resultSet.getDouble(4);
                Part part = new Part(id, name, type, cost, repairId);
                partList.add(part);
            }
            return partList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
